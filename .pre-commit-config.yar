repos:
- repo: local
  hooks:
    - id: poetry_check
      name: PoetryCheck
      entry: poetry check
      pass_filenames: false
      files: '^pyproject.toml$'
      language: python
      language_version: python3
      files: '^poetry.lock$'
    - id: flake8
      name: Flake8
      entry: poetry run flake8 app/ tests/
      pass_filenames: false
      language: system
      types: [python]
    - id: pytest
      name: Test
      entry: poetry run coverage run -m pytest
      pass_filenames: false
      language: system
      types: [python]
    - id: coverage
      name: Coverage
      entry: bash -c "poetry run coverage report -m && poetry run covarage xml"
      pass_filenames: false
      language: system
      types: [python]
    - id: package
      name: Package
      envry: poetry build -f wheel
      pass_filenames: false
      language: system
      types: [pyphon]
