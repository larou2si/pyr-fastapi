#  Documentation RESTful API Server with Python, SQLAlchemy, FastAPI, and PostgreSQL

- todo: filtration du posts (updated_at__le...)
- todo: terminer les tests

## Variables d'environnement


## Développer le projet

Avant de commencer le development du projet, il faut suivre ces commandes.

- Garantir l'installation de Poetry : https://python-poetry.org/docs/master/#installation
- Assurer la création de ton GITLAB_ACCESS_*

```
# Installation des dependances
poetry install
# Installation pre-commit, pour mieux rassurer le CI
poetry run pre-commit install
poetry run pre-commit migrate-config
```

make migrations of the models created or modified in this project
- alembic init alembic
- alembic reversion --autogeneration head
  Maintenant, vous pouvez commencer le dévelopement :)
- Lancement de l'app en local: `poetry run uvicorn app.main:app`

- Exécuter et assurer les tests du projet avant de télécharger le code à gitlab
```
poetry run coverage run -m pytest
poetry run coverage report -m
```