import pytest
from app import db, settings


@pytest.fixture
async def init_db():
    conn = await db.set_bind(settings.DATABASE_URI)
    yield conn
    await conn.close()