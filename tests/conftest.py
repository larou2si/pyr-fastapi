import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy import create_engine
from app.database import Base
from app.main import app


@pytest_asyncio.fixture
async def test_client():
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client


@pytest.fixture(scope="function", autouse=True)
def db_session():
    from app import db
    Base.metadata.drop_all(bind=db.engine)
    Base.metadata.create_all(bind=db.engine)
    yield db.get_session()
    Base.metadata.drop_all(bind=db.engine)
