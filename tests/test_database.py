from sqlalchemy.orm import Session
from app import db
from app.database import DBConnector


def check_readiness(session: Session):
    assert session.is_active
    res = session.execute("select 1").fetchone()[0]
    assert res


def test_default_db():
    assert db
    assert isinstance(db, DBConnector)


def test_get_session_db_connector():
    session = next(db.get_session())
    with session as transaction:
        check_readiness(transaction)
