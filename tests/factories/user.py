
from factory import Sequence, alchemy

from app.models import User


class UserFactory(alchemy.SQLAlchemyModelFactory):
    id = Sequence(lambda n: n)

    class Meta:
        model = User
