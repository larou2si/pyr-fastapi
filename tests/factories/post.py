import uuid
from factory import Sequence, alchemy

from app.models import Post


class PostFactory(alchemy.SQLAlchemyModelFactory):
    id = Sequence(lambda n: uuid.uuid4())

    class Meta:
        model = Post
