import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.orm import Session


@pytest.mark.asyncio
async def test_version(test_client: AsyncClient) -> None:
    response = await test_client.get("/api/version")
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_liveness(test_client: AsyncClient):
    response = await test_client.get("/api/liveness")
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.asyncio
async def test_readiness_ok(test_client: AsyncClient, db_session: Session):
    response = await test_client.get("/api/readiness")
    assert response.status_code == status.HTTP_200_OK