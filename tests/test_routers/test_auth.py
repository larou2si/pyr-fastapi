import pytest
from tests import init_db

from sqlalchemy.orm import Session

from app.schemas import CreateUserSchema

@pytest.mark.asyncio
async def test_create_user(test_client,init_db):
    user = CreateUserSchema(
        name= "testuser1",
        email= "testuser1@nofoobar.com",
        password= "testing123",
        passwordConfirm= "testing123",
        photo= "testing"
        )
    response = await test_client.post("/api/auth/signup", json=user.dict())
    assert response.status_code == 201
    