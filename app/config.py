from dataclasses import Field
from pydantic import BaseSettings, PostgresDsn


class PostgresConfig(BaseSettings):
    POSTGRES_DB: str #= Field(env="POSTGRES_DB")
    DATABASE_PORT: str #= Field(env="DATABASE_PORT")
    POSTGRES_PASSWORD: str #= Field(env="POSTGRES_PASSWORD")
    POSTGRES_USER: str #= Field(env="POSTGRES_USER")
    POSTGRES_HOSTNAME: str #= Field(env="POSTGRES_HOSTNAME")

    @property
    def database(self) -> str:
        return PostgresDsn.build(
            scheme="postgresql",
            user=self.POSTGRES_USER,
            password=self.POSTGRES_PASSWORD,
            host=self.POSTGRES_HOSTNAME,
            port=self.DATABASE_PORT,
            path=f"/{self.POSTGRES_DB}",
        )
    
    class Config:
        env_file = './.env'


class Settings(PostgresConfig):

    JWT_PUBLIC_KEY: str
    JWT_PRIVATE_KEY: str
    REFRESH_TOKEN_EXPIRES_IN: int
    ACCESS_TOKEN_EXPIRES_IN: int
    JWT_ALGORITHM: str

    CLIENT_ORIGIN: str

    VERIFICATION_SECRET: str
