from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from sqlalchemy import select
from sqlalchemy.orm import Session
from app import db as get_db

from app.models import User


router = APIRouter()

@router.get("/version")
async def version():
    from app import __version__
    return JSONResponse(status_code=200, content={"response":__version__})


@router.get("/liveness")
async def liveness():
    return JSONResponse(status_code=200, content={"response":"ok"})


@router.get("/readiness")
async def readiness(db_session: Session = Depends(get_db.get_session)):
    with db_session as transaction:
        transaction.execute(select(User)).first()
    return JSONResponse(status_code=200, content={"response":"ok"})