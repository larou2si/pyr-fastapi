from typing import Any, Dict
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.orm import sessionmaker, Session
"""from .config import settings

SQLALCHEMY_DATABASE_URL = f"postgresql://{settings.POSTGRES_USER}:{settings.POSTGRES_PASSWORD}@{settings.POSTGRES_HOSTNAME}:{settings.DATABASE_PORT}/{settings.POSTGRES_DB}"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()"""


class_registry: Dict = {}


@as_declarative(class_registry=class_registry)
class Base:  # pragma: no cover
    id: Any
    __name__: str

class DBConnector:
    """Setup sqlalchemy database connection."""

    def __init__(self, db_url: str):
        # TODO add log
        self._db_dns: str = db_url
        self.engine = create_engine(self._db_dns)

    def get_session(self) -> Session:
        # create session factory
        session_maker = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        yield session_maker.begin()
