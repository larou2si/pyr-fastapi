from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app import settings
from app.routers import auth, post, health_check

app = FastAPI(title="NW Groupe")

origins = [
    settings.CLIENT_ORIGIN,
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(health_check.router, tags=['Health Check'], prefix='/api')
app.include_router(auth.router, tags=['Auth'], prefix='/api/auth')
app.include_router(post.router, tags=['Posts'], prefix='/api/posts')
