from pkg_resources import DistributionNotFound, get_distribution
import warnings

from app.config import Settings
from app.database import DBConnector

__pkg_name__ = "pyr-fastapi"


try:
    __version__ = get_distribution(__pkg_name__).version
except DistributionNotFound as err:
    __version__ = "@dev"
    warnings.warn(f"{__pkg_name__} not installled, __version__ : {__version__}")


settings = Settings()
db = DBConnector(settings.database)
